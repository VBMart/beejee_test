<?php

include_once 'func.php';
include_once 'config.php';
include_once 'class.task.php';

define('task_table_name', 'tasks');

class TaskList {
    /* @var $items Task[] */
    public $items;
    private $mysqli;

    public function __construct() {
        $this->mysqli = new mysqli(db_host, db_user, db_pass, db_name, db_port);
        // TODO: Check connection error ($this->mysqli->connect_errno)
    }

    private function parseMysqlResult($mysqlRes) {
        $this->items = [];

        while ($row = $mysqlRes->fetch_assoc()) {
            $task = new Task($row);
            array_push($this->items, $task);
        }

        return count($this->items);
    }

    private function loadBySqlQuery($sql) {
        $res = mysqli_query($this->mysqli, $sql);
        // TODO: Check query result

        return $this->parseMysqlResult($res);
    }

    public function loadAll() {
        $sql = 'SELECT * FROM `'.task_table_name.'`';
        return $this->loadBySqlQuery($sql);
    }

    public function loadPart($from, $limit, $order = '', $direction = '') {
        $sql = 'SELECT * FROM `'.task_table_name.'`';
        if ($order !== '') {
            if ($direction == '') {
                $direction = 'ASC';
            }
            $sql .= sprintf(' ORDER BY `%s` %s', $order, $direction);
        }
        $sql .= sprintf(' LIMIT %s, %s', $from, $limit);

        return $this->loadBySqlQuery($sql);
    }

    public function getTasksCount() {
        $sql = 'SELECT count(id) FROM `'.task_table_name.'`';
        $res = mysqli_query($this->mysqli, $sql);
        $row = $res->fetch_row();
        return intval($row[0]);
    }

    /* @var $task Task */
    public function addTask($task) {
        $sql = $this->mysqli->prepare('INSERT INTO `'.task_table_name.'` (`user_name`, `email`, `text`) VALUES (?, ?, ?)');
        $sql->bind_param('sss', $task->userName, $task->email, $task->text);
        return $sql->execute();
    }
}