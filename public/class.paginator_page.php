<?php

class PaginatorPage {
    public $isActive;
    public $url;
    public $caption;

    public function __construct() {
        $this->isActive = false;
        $this->url = '';
        $this->caption = '';
    }
}