<?php

include_once 'class.task_list.php';
include_once 'func.php';

class AddTaskViewer {

    public $taskId;
    public $userName;
    public $email;
    public $text;
    public $isPostValid;
    public $action;
    public $errorMessages;
    public $isTaskAdded;

    public function __construct() {
        if (count($_POST) == 0) {
            $this->isPostValid = null;
            $this->isTaskAdded = null;
        } else {
            $this->userName = $this->getInput('user_name');
            $this->email = $this->getInput('email');
            $this->text = $this->getInput('text');
            $this->action = $this->getInput('action');

            $this->checkInputs();
            if ($this->isPostValid) {
                $taskList = new TaskList();
                $task = new Task();
                $task->userName = $this->userName;
                $task->email = $this->email;
                $task->text = $this->text;

                $this->isTaskAdded = $taskList->addTask($task);
            }
        }
    }

    private function getInput($inp) {
        if (array_key_exists($inp, $_POST)) {
            return stripcslashes($_POST[$inp]);
        } else {
            return '';
        }
    }

    private function checkInputs() {
        $this->isPostValid = true;
        $this->errorMessages = [];

        if ($this->userName == '') {
            array_push($this->errorMessages, 'User name must be not empty.');
            $this->isPostValid = false;
        }
        if ($this->email == '') {
            array_push($this->errorMessages, 'Email must be not empty.');
            $this->isPostValid = false;
        } else {
            if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
                array_push($this->errorMessages, 'Incorrect email.');
                $this->isPostValid = false;
            }
        }
        if ($this->text == '') {
            array_push($this->errorMessages, 'Text must be not empty.');
            $this->isPostValid = false;
        }

        if ($this->action == 'edit') {
            // TODO: Check is_admin
        }
    }

    public function isFormVisible() {
        if (is_null($this->isPostValid)) {
            return true;
        }

        if ($this->isPostValid === false) {
            return true;
        }

        if ($this->isTaskAdded === false) {
            return true;
        }

        return false;
    }
}