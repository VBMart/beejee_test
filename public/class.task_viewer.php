<?php

include_once 'func.php';
include_once 'config.php';
include_once 'class.task_list.php';
include_once 'class.paginator_page.php';
include_once 'class.task_table_header_item.php';

class TaskViewer {

    /* @var $tasks TaskList */
    public $tasks;
    public $taskCount;
    public $limit;
    public $from;
    public $order;
    public $direction;

    public $pageCount;
    public $currentPage;
    /* @var $pages PaginatorPage[] */
    public $pages;
    /* @var $pages TaskTableHeaderItem[] */
    public $taskTableHeader;

    public function __construct() {
        $this->tasks = new TaskList();
        $this->taskCount = $this->tasks->getTasksCount();

        $this->limit = 3;
        $this->from = 0;

        if (isset($_GET['from'])) {
            $this->from = intval($_GET['from']);
        }

        $this->direction = 'asc';
        if (isset($_GET['direction'])) {
            $dir = strtolower($_GET['direction']);
            if (in_array($dir, ['asc', 'desc'])) {
                $this->direction = $dir;
            }
        }

        $this->order = 'user_name';
        if (isset($_GET['orderby'])) {
            $order = strtolower($_GET['orderby']);
            if (in_array($order, ['user_name', 'email', 'is_done'])) {
                $this->order = $order;
            }
        }

        $this->taskTableHeader = [];

        $headerItems = [
            ['caption' => 'User name', 'order' => 'user_name'],
            ['caption' => 'E-mail', 'order' => 'email'],
            ['caption' => 'Text', 'order' => ''],
            ['caption' => 'status', 'order' => 'is_done'],
        ];
        foreach ($headerItems as $item) {
            $headerItem = new TaskTableHeaderItem();
            $headerItem->caption = $item['caption'];
            $order = $item['order'];
            $headerItem->url = '';
            if ($order != '') {
                $headerItem->url = $this->generateLink($order, $this->generateDirection($order), -1);
                if ($order == $this->order) {
                    $headerItem->direction = $this->generateDirection($order);
                }
            }
            array_push($this->taskTableHeader, $headerItem);
        }

        $this->tasks->loadPart($this->from, $this->limit, $this->order, $this->direction);

        $this->pageCount = $this->taskCount / $this->limit;
        $this->currentPage = $this->from / $this->limit;
        $this->pages = [];
        if ($this->isPaginatorVisible()) {
            for ($iPage = 0; $iPage < $this->pageCount; $iPage++){
                $paginatorPage = new PaginatorPage();
                $paginatorPage->isActive = $this->currentPage == $iPage;
                $paginatorPage->caption = $iPage;
                $paginatorPage->url = $this->generateLink('', '', $this->limit*$iPage);
                array_push($this->pages, $paginatorPage);
            }
        }
    }

    public function isPaginatorVisible() {
        return $this->taskCount > $this->limit;
    }

    private function generateLink($order = '', $direction = '', $from = -1) {
        if ($order == '') {
            $order = $this->order;
        }
        if ($direction == '') {
            $direction = $this->direction;
        }
        if ($from == -1) {
            $from = $this->from;
        }

        $url = sprintf('%s?orderby=%s&direction=%s&from=%s', site_url, $order, $direction, $from);
        return $url;
    }

    private function generateDirection($order) {
        if ($order != $this->order) {
            return 'asc';
        } else {
            if ($this->direction == 'asc') {
                return 'desc';
            } else {
                return 'asc';
            }
        }
    }
}