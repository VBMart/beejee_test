<?php include_once 'config.php'; ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Task list</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="<?= site_url; ?>">Task list</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <?php
                include_once 'class.menu.php';
                $menu = new Menu();

                /* @var $item MenuItem */
                foreach ($menu->items as $item) {
                    $activeText = '';
                    if ($item->isActive){
                        $activeText = ' active';
                    }
                    ?>
                    <li class="nav-item<?= $activeText; ?>">
                        <a class="nav-link" href="<?= $item->url; ?>"><?= $item->caption; ?></a>
                    </li>
                    <?php
                }
            ?>
        </ul>
    </div>
</nav>
<div class="container">