<?php

class TaskTableHeaderItem {
    public $direction;
    public $url;
    public $caption;

    public function __construct() {
        $this->direction = '';
        $this->url = '';
        $this->caption = '';
    }
}