<?php

include_once 'header.php';
include_once 'class.addtask_viewer.php';

$addTaskViewer = new AddTaskViewer();

if ($addTaskViewer->isPostValid === false) {
    echo '<p>Invalid inputs:</p>';
    echo '<ul>';
    foreach ($addTaskViewer->errorMessages as $errorMessage) {
        echo sprintf('<li>%s</li>', $errorMessage);
    }
    echo '</ul>';
}

if ($addTaskViewer->isTaskAdded === true) {
    echo '<p>Task added.</p>';
}
if ($addTaskViewer->isTaskAdded === false) {
    echo '<p>Can\'t add task. Please try again later.</p>';
}

if ($addTaskViewer->isFormVisible()) {
?>
    <form method="post" action="addtask.php">
        <div class="form-group">
            <label for="userName">User name</label>
            <input type="text" class="form-control" id="userName" name="user_name"
                   value="<?= $addTaskViewer->userName; ?>">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" value="<?= $addTaskViewer->email; ?>">
        </div>
        <div class="form-group">
            <label for="taskText">Task text</label>
            <input type="text" class="form-control" id="taskText" name="text" value="<?= $addTaskViewer->text; ?>">
        </div>
        <input type="hidden" name="action" value="add">
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
<?php
}

include_once 'footer.php';