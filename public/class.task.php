<?php

class Task {

    public $id;
    public $userName;
    public $email;
    public $text;
    public $isDone;
    public $isEdited;

    public function __construct($row = null) {
        if (!is_null($row)) {
            $this->fillByDbRow($row);
        }
    }

    private function fillByDbRow($row){
        $this->id = $row['id'];
        $this->userName = $row['user_name'];
        $this->email = $row['email'];
        $this->text = $row['text'];
        $this->isDone = $row['is_done'] == 1;
        $this->isEdited = $row['is_edited'] == 1;
    }
}