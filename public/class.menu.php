<?php

include_once 'config.php';
include_once 'class.menu_item.php';

class Menu {
    /* @var $items MenuItem[] */
    public $items;

    public function __construct() {
        $this->items = [];

        $item = new MenuItem();
        $item->caption = 'Home';
        $item->isActive = $this->isFrontPage();
        $item->url = site_url;
        array_push($this->items, $item);

        $item = new MenuItem();
        $item->caption = 'New task';
        $item->isActive = $this->isNewTaskPage();
        $item->url = site_url . 'addtask.php';
        array_push($this->items, $item);

        if ($this->isAdminPage()){
            $item = new MenuItem();
            $item->caption = 'Admin';
            $item->isActive = True;
            array_push($this->items, $item);

            $item = new MenuItem();
            $item->caption = 'Log out';
            $item->isActive = False;
            $item->url = site_url . 'logout.php';
            array_push($this->items, $item);
        }else{
            $item = new MenuItem();
            $item->caption = 'Login';
            $item->isActive = $this->isLoginPage();
            if (!$item->isActive) {
                $item->url = site_url . 'login.php';
            }
            array_push($this->items, $item);
        }
    }

    public function isAdminPage() {
        return false;
    }

    public function isFrontPage() {
        return true;
    }

    public function isLoginPage() {
        return false;
    }

    public function isNewTaskPage() {
        return false;
    }
}