<?php

function my_var_dump($var, $comment = '') {
    ob_start();
    var_dump( $var );
    $result = ob_get_clean();
    if ($comment != ''){
        $comment = '<p>'.$comment.'</p>';
    }
    return '<pre>'.$comment.$result.'</pre>';
}