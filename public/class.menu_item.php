<?php

class MenuItem {
    public $url;
    public $isActive;
    public $caption;

    public function __construct() {
        $this->url = '';
        $this->caption = '';
        $this->isActive = false;
    }
}