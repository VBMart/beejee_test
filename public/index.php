<?php

include_once 'header.php';

include_once 'func.php';
include_once 'config.php';
include_once 'class.task_viewer.php';

$taskViewer = new TaskViewer();

?>
    <table class="table table-hover">
        <thead>
        <tr>
            <?php
            /* @var $item TaskTableHeaderItem */
            foreach ($taskViewer->taskTableHeader as $item) {
                $dirIcon = '';
                if ($item->direction == 'asc') {
                    $dirIcon = '▼';
                }
                if ($item->direction == 'desc') {
                    $dirIcon = '▲';
                }
                if ($item->url != '') {
                    echo sprintf('<th scope="col"><a href="%s">%s %s</a></th>', $item->url, $item->caption, $dirIcon);
                } else {
                    echo sprintf('<th scope="col">%s</th>', $item->caption);
                }
            }
            ?>
        </tr>
        </thead>
        <tbody>

        <?php
        /* @var $task Task */
        foreach ($taskViewer->tasks->items as $task) {
            ?>
            <tr>
                <th scope="row"><?= $task->userName; ?></th>
                <td><?= $task->email; ?></td>
                <td>
                    <?php
                    echo(htmlspecialchars($task->text));
                    if ($task->isEdited) {
                        echo ' <span class="badge badge-warning">Edited by Admin</span>';
                    }
                    ?>
                </td>
                <td><?= ($task->isDone ? '<span class="badge badge-success">Done</span>' : '<span class="badge badge-secondary">In progress</span>') ?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
<?php

if ($taskViewer->isPaginatorVisible()) {
    ?>
    <nav aria-label="Task navigation">
        <ul class="pagination">
            <?php
            foreach ($taskViewer->pages as $page) {
                $active = '';
                if ($page->isActive) {
                    $active = ' active';
                }
                echo(sprintf('<li class="page-item %s"><a class="page-link" href="%s">%s</a></li>', $active, $page->url, $page->caption));
            }
            ?>
        </ul>
    </nav>
    <?php
}

include_once 'footer.php';